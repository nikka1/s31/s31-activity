// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute")
	// This will allow us to use all the routes defined in the "taskRoute.js"



// Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.jtm9g.mongodb.net/B157_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true	
	});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Add the task route
// Allows all the task routes created in the "taskRoute.js" to use the "/tasks" route
app.use("/tasks", taskRoute);
	// http://localhost:4000/tasks/

app.listen(port, () => console.log(`Now listening to port ${port}`));